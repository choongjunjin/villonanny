package net.villonanny.entity;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.InstanceCreator;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonParser;

import net.villonanny.ConversationException;
import net.villonanny.EventLog;
import net.villonanny.Translator;
import net.villonanny.Util;
import net.villonanny.entity.SevenBySeven.OutputType;
import net.villonanny.misc.Coordinates;
import net.villonanny.type.OasisType;
import net.villonanny.type.ResourceType;
import net.villonanny.type.TribeType;
import net.villonanny.type.ValleyType;

/**
 * The Map Entity's goal is to keep mapping in an organised manner.
 * This class houses information on map tiles, which could further be classified as Oases, Village,
 * Unoccupied Valley, etc.
 * 
 * Since Travian T4 has introduced Zoom Level. It's appropriate for us to house tiles within a
 * zooming distance. Therefore the Map instance should support up to 961 holding of valley (tile).
 * 
 * @version 1.0 T4
 * @author Zepx
 *
 */
public class TravianMap {
	private final static Logger log = Logger.getLogger(TravianMap.class);	
	
	public final static int MIN_RADIUS = 1; 
	public final static int MAX_TILES = 961;
	public final static ZoomType MAX_ZOOM = ZoomType.ZOOM3;	
	private final static String AJAX_URL = "ajax.php?cmd=mapPositionData";
	
	private Util util;
	private Translator translator;
	private ArrayList<JsonTile> tileList;
	private Coordinates coord;
	
	public enum ZoomType {		
		ZOOM1(1, 4, 11, 9),
		ZOOM2(2, 8, 21, 17),
		ZOOM3(3, 15, 31, 31);
		
		private int radius;
		private int zoom;
		private int lengthX;
		private int lengthY;
		
		private ZoomType(int zoom, int radius, int lengthX, int lengthY) {
			this.radius = radius;
			this.zoom = zoom;
			this.lengthX = lengthX;
			this.lengthY = lengthY;
		}
		
		public int getRadius() {
			return radius;
		}
		
		public int getZoom() {
			return zoom;
		}
		
		public int getLengthX() {
			return lengthX;
		}
		
		public int getLengthY() {
			return lengthY;
		}
	}
	
	public enum OutputType {
		// output file types, default is text
		CSV("csv"),
		XML("xml"),
		TXT("txt")
	    ;
		
		private String fileExt;
		
		OutputType (String fileExt) {
			// use this to set the class value from parameter
			this.fileExt = fileExt;
		}
		
		public String getExtension() {
			return fileExt;
		}
		
		public String toString() {
			return fileExt;			
		}
		
		private final static Map<String, OutputType> fromStringMap;
		static {
			fromStringMap = new HashMap<String, OutputType>();
			OutputType[] values = OutputType.values();
			for (int i = 0; i < values.length; i++) {
				OutputType m = values[i];
				fromStringMap.put(m.fileExt.toLowerCase(), m);
			}
		}
		
		public static OutputType fromKey(String key) {
			if (key==null) {
				return null;
			}
			String lowKey = key.toLowerCase();
			OutputType result = fromStringMap.get(lowKey);
			if (result == null) {
				EventLog.log("Unrecognised File Type - " + key);
			}
			return result;
		}	
	}
	
	/**
	 * Calculates the the distance between two points
	 * 
	 * @param x coordinate x
	 * @param y coordinate y
	 * @param x2 coordinate x2
	 * @param y2 coordinate y2
	 * @return distance between two points
	 */
	public static double calculateDistance(int x, int y, int x2, int y2) {
		double xDiff = x2 - x;
		double yDiff = y2 - y;
		double distance = Math.sqrt(Math.pow(xDiff, 2) + Math.pow(yDiff, 2));
		return distance;
	}
	
	/**
	 * Constructor for map object. It accepts the utility object for convenience purposes.
	 * 
	 * @param util
	 */
	public TravianMap(Util util, Translator translator) {
		this.util = util;
		this.translator = translator;
		tileList = new ArrayList<JsonTile>();
	}
	
	/**
	 * This function scans the map at a specific zoom level and returns a list of squares
	 * within the given range (zoom and radius).
	 * 
	 * By default this function uses the largest zoom possible (zoom level 3).
	 * 
	 * Zoom indicates the amount of data fetched from the ajax page at one time. The limit is Zoom 3.
	 * Zoom 1 - 99 Squares
	 * Zoom 2 - 357 Squares 
	 * Zoom 3 - 961 Squares
	 * 
	 * Radius determines the number of squares it will search starting from the given point x, y.
	 * 
	 * @param x starting point x
	 * @param y starting point y
	 * @param zoom the amount of zoom available (min 1, max specified by MAX_RADIUS)
	 * @param radius the distance from point x and y
	 * @throws ConversationException 
	 */
	public List<JsonTile> fetchRadius(int x, int y, int radius) throws ConversationException {
		//Obtain most suitable zoom.
		ZoomType zoom = MAX_ZOOM;
		for(ZoomType t : ZoomType.values()) {
			if(radius <= t.getRadius())
				zoom = t;
		}
		
		boolean finish = false;
		//Obtain bottom corner from the center point x, y. Start by obtaining squares around this corner.
		int lastX = (int) (x - (Math.floor(radius / zoom.getLengthX()) * zoom.getLengthX()));
		int lastY = (int) (y - (Math.floor(radius / zoom.getLengthY()) * zoom.getLengthY()));
		
		tileList.clear();
		while(!finish) {
			List<JsonTile> jsonTiles = fetchTiles(lastX, lastY, zoom);
			
			//Checks the distance of tiles. Exclude them if it's more than radius	
			for(JsonTile tile : jsonTiles) {
				if(Math.floor(calculateDistance(tile.x(), tile.y(), x, y)) <= radius) {
					//Valley v = new Valley(util, tile.x(), tile.y());
					tile.init(); //Initialise tiles first
					tileList.add(tile);
					
				}
				
				if(tile.x() == -52 && tile.y() == -90)
					System.out.println("Tile found");
			}
			
			// Advance along the row. Move to the next row when done.
            lastX += zoom.getLengthX();
            if (lastX > (x + radius)) {
                // step Y down by MAX_ZOOM.getRadius().
                lastY += zoom.getLengthY();
                if (lastY > (y + radius)) {
                    // mark done
                    lastX = lastY = -999;
                    finish = true;
                    EventLog.log("Finished Searching");
                } else {
                    lastX = x - radius;
                }
            }
		}
		
		//Return the constructed an immutable list
		return Collections.unmodifiableList(tileList);
	}
	
	/* TODO: Calculation that obtains squares e.g 3x3, 5x5, 7x7. Must be odd and must always have a centre point
	public List<JsonTile> fetchSquare(int x, int y, int zoom, int multiple) throws ConversationException {
		
		
		List<JsonTile> jsonTiles = fetchTiles(x, y, zoom);
		
		tileList.clear();
		for(JsonTile tile: jsonTiles) {
			
		}
	}
	*/
	
	/**
	 * This function returns a list of tiles given the zoom level. Unlike fetchRadius(),
	 * it does not return tiles in circular fashion, instead returns the raw tiles fetched from
	 * Travian itself.
	 * 
	 * Zoom indicates the amount of data fetched from the ajax page at one time. The limit is Zoom 3.
	 * Zoom 1 - 99 Squares - Radius 4
	 * Zoom 2 - 357 Squares - Radius 8
	 * Zoom 3 - 961 Squares - Radius 15
	 * 
	 * Radius determines the number of squares it will search starting from the given point x, y.
	 * 
	 * @param x x coordinate
	 * @param y y coordinate
	 * @param zoom the amount of zoom available
	 * @param radius the distance from point x and y
	 * @throws ConversationException 
	 */
	public List<JsonTile> fetchTiles(int x, int y, ZoomType zoom) throws ConversationException {
		if(zoom == null)
			zoom = MAX_ZOOM;
		
		String mapUrl = Util.getFullUrl(util.getBaseUrl(), "karte.php");
		String ajaxUrl = Util.getFullUrl(util.getBaseUrl(), AJAX_URL);
		
		//Construct the POST data
		String[] postNames = { "cmd", "data[x]", "data[y]", "data[zoomLevel]" };
		String[] postValues = { "mapPositionData", String.valueOf(x), String.valueOf(y), String.valueOf(zoom.getZoom()) };
		
		//To simulate page order, we first head to karte.php
		String page = util.httpGetPage(mapUrl);
		page = util.httpPostPage(ajaxUrl, Arrays.asList(postNames), Arrays.asList(postValues), true);
		
		//parse the response
		GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder.registerTypeAdapter(JsonTile.class, new TileInstance());
		
		JsonObject obj = new JsonParser().parse(page).getAsJsonObject();
		JsonArray array = obj.getAsJsonObject("data").getAsJsonArray("tiles");
		
		Gson gson = gsonBuilder.create();
		JsonTile[] jsonTiles = gson.fromJson(array, JsonTile[].class);
		
		return Arrays.asList(jsonTiles);
	}
	
	public void setOutputType(OutputType outputType, Boolean fileAppend) {
		// TODO Auto-generated method stub		
	}
	
	public void setOutputType(OutputType outputType) {
		// TODO Auto-generated method stub		
	}
	
	/**
	 * This class is reflected using google Gson library.
	 * It automatically places the values taken from ajax.php?cmd=mapPositionData POST request.
	 * Some of the data may be 0 or null, thus specifying that the data is unavailable.
	 * Here's a reference extracted from the javascript deobfuscator.
	 * 
	 * TODO: Translation
	 * 
	 * Travian.Translation.add({'k.spieler': "Player", 'k.einwohner': "Population",
	 *  'k.allianz': "Alliance", 'k.volk': "Tribe", 'k.dt': "Village", 'k.bt': "Occupied oasis",
	 *  'k.fo': "Unoccupied oasis", 'k.vt': "Abandoned valley", 'k.loadingData': "Loading...",
	 *  'a.v1': "Romans", 'a.v2': "Teutons", 'a.v3': "Gauls", 'a.v4': "Nature", 'a.v5': "Natars", 
	 *  'k.f1': "3-3-3-9", 'k.f2': "3-4-5-6", 'k.f3': "4-4-4-6", 'k.f4': "4-5-3-6", 'k.f5': "5-3-4-6", 
	 *  'k.f6': "1-1-1-15", 'k.f7': "4-4-3-7", 'k.f8': "3-4-4-7", 'k.f9': "4-3-4-7", 'k.f10': "3-5-4-6", 
	 *  'k.f11': "4-3-5-6", 'k.f12': "5-4-3-6", 'k.f99': "Natarian village", 'b.ri1': "Won as attacker without losses.", 
	 *  'b.ri2': "Won as attacker with losses.", 'b.ri3': "Lost as attacker.", 'b.ri4': "Won as defender without losses.", 
	 *  'b.ri5': "Won as defender with losses.", 'b.ri6': "Lost as defender with losses.", 
	 *  'b.ri7': "Lost as defender without losses.", 'b:ri1': "&lt;img src=\"img/x.gif\" class=\"iReport iReport1\"/&gt;".unescapeHtml(), 
	 *  'b:ri2': "&lt;img src=\"img/x.gif\" class=\"iReport iReport2\"/&gt;".unescapeHtml(), 
	 *  'b:ri3': "&lt;img src=\"img/x.gif\" class=\"iReport iReport3\"/&gt;".unescapeHtml(), 
	 *  'b:ri4': "&lt;img src=\"img/x.gif\" class=\"iReport iReport4\"/&gt;".unescapeHtml(), 
	 *  'b:ri5': "&lt;img src=\"img/x.gif\" class=\"iReport iReport5\"/&gt;".unescapeHtml(), 
	 *  'b:ri6': "&lt;img src=\"img/x.gif\" class=\"iReport iReport6\"/&gt;".unescapeHtml(), 
	 *  'b:ri7': "&lt;img src=\"img/x.gif\" class=\"iReport iReport7\"/&gt;".unescapeHtml(), 
	 *  'b:bi0': "&lt;img class=\"carry empty\" src=\"img/x.gif\" alt=\"Bounty\" /&gt;".unescapeHtml(), 
	 *  'b:bi1': "&lt;img class=\"carry half\" src=\"img/x.gif\" alt=\"Bounty\" /&gt;".unescapeHtml(), 
	 *  'b:bi2': "&lt;img class=\"carry\" src=\"img/x.gif\" alt=\"Bounty\" /&gt;".unescapeHtml(), 
	 *  'a.r1': "Lumber", 'a.r2': "Clay", 'a.r3': "Iron", 'a.r4': "Crop", 
	 *  'a:r1': "&lt;img alt=\"Lumber\" src=\"img/x.gif\" class=\"r1\"&gt;".unescapeHtml(), 
	 *  'a:r2': "&lt;img alt=\"Clay\" src=\"img/x.gif\" class=\"r2\"&gt;".unescapeHtml(), 
	 *  'a:r3': "&lt;img alt=\"Iron\" src=\"img/x.gif\" class=\"r3\"&gt;".unescapeHtml(), 
	 *  'a:r4': "&lt;img alt=\"Crop\" src=\"img/x.gif\" class=\"r4\"&gt;".unescapeHtml(), 
	 *  'k.arrival': "arrival at", 'k.ssupport': "reinforcement", 'k.sspy': "scouting", 
	 *  'k.sreturn': "return", 'k.sraid': "raid", 'k.sattack': "attack"});
	 * 
	 * @author Zepx
	 */
	public static class JsonTile {
		private int x, y, u, a, d;
		private String c;
		private String t;		
		
		transient private Util util;
		transient private Translator translator;
		transient private String name, alliance, population;
		transient private ValleyType valleyType;
		transient private Coordinates coord;
		transient private OasisType oasis;
		transient private TribeType tribe;
		
		public JsonTile() { };
		
		/**
		 * JsonTile requires utility object for self creating other variables.
		 * 
		 * @param util
		 */
		public JsonTile(Util util, Translator translator) {
			this.util = util;
			this.translator = translator;
		}
		
		public void init() {
			coord = new Coordinates(xStr(), yStr());
			Pattern  p;
			Matcher m;
			
			//Determine the type of valley
			//Village, Unoccupied Oasis, Occupied Oasis, Abandoned Valley or Empty Tile  
			if(c != null) {
				if(c.contains("{k.fo}")) { // Unoccupied Oasis
					p = util.getPattern("map.valley.oasis");
					m = p.matcher(t);
					
					if(m.find()) {
						ResourceType res1, res2 = null;
						
						res1 = ResourceType.fromInt(Integer.parseInt(m.group(1)) - 1);						
						
						if(m.group(3) != null && m.group(4) != null) {
							res2 = ResourceType.fromInt(Integer.parseInt(m.group(3)) - 1);							
						}
						
						oasis = OasisType.fromString(res1, res2);
						name = "Unoccupied Oasis";
					}
					
				} else if(c.contains("{k.bt}")) { // Occupied Oasis
					//TODO: No data yet
				} else if(c.contains("{k.vt}")) { // Abandoned Valley
					p = util.getPattern("map.valley.abandonedValley");
					m = p.matcher(c);
					
					if(m.find()) {
						valleyType = ValleyType.fromInt(Integer.parseInt(m.group(1)));
						name = "Abandoned Valley";
					}
				} else if(c.contains("{k.dt}")) { // Village
					p = util.getPattern("map.valley.village");
					m = p.matcher(t);
					
					if(m.find()) {
						name = m.group(1);
						population = m.group(2);
						alliance = m.group(3);
						tribe = TribeType.fromInt(Integer.parseInt(m.group(4)) - 1);
					}
					
				}
				
			}
		}
		
		public int x() { return x; }
		public String xStr() { return String.valueOf(x); }
		public int y() { return y; }
		public String yStr() { return String.valueOf(y); }
		public int uid() { return u; } //user id spieler?uid=????
		public int aid() { return a; } //alliance id allianz?aid=???
		public int did() { return d; } //coordinate to id aka coordinate2mapId
		public String text() { return t; } //text
		
		/*
		 * Getters
		 */
		public String getName() { return name; } 
		public String getAllianceName() { return alliance; }
		public int getPopulation() { return Integer.parseInt(population); }
		public ValleyType getValleyType() { return valleyType; }
		
		
		public Coordinates getCoordinates() {
			return coord;
		}
		
		public boolean isEmptyTile() {
			return c == null;
		}
		
		public boolean isVillage() {
			return tribe != null;
		}
		
		public boolean isOasis() {
			return oasis != null;
		}
		
		public boolean isValley() { 
			return valleyType != null;
		}
	}
	
	public class TileInstance implements InstanceCreator<JsonTile> {

		@Override
		public JsonTile createInstance(Type type) {
			return new JsonTile(util, translator);
		}
		
	}

}
