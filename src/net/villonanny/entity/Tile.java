package net.villonanny.entity;

import org.apache.log4j.Logger;

import net.villonanny.misc.Coordinates;

public class Tile {
	private static final Logger log = Logger.getLogger(Tile.class);
	
	private Coordinates coord;
	
	public Tile(int x, int y) {
		coord = new Coordinates(String.valueOf(x), String.valueOf(y));
	}
	
	public Tile(Coordinates coord) {
		this.coord = coord;
	}
	
	
	

}
