package net.villonanny.strategy;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.configuration.SubnodeConfiguration;
import org.apache.log4j.Logger;
import org.apache.log4j.NDC;

import net.villonanny.ConversationException;
import net.villonanny.EventLog;
import net.villonanny.InvalidConfigurationException;
import net.villonanny.TimeWhenRunnable;
import net.villonanny.Util;
import net.villonanny.entity.SevenBySeven;
import net.villonanny.entity.TravianMap;
import net.villonanny.entity.TravianMap.JsonTile;
import net.villonanny.entity.TravianMap.OutputType;
import net.villonanny.entity.Village;
import net.villonanny.misc.Coordinates;
import net.villonanny.type.ValleyType;

/**
 * Cropfinder for T4.
 * 
 * The cropfinder can find any type of abandoned valley with given number of crop fields you would like
 * In addition to the finding. It will check for type of oasis available in a 3 square radius.
 * 
 * @author Zepx
 *
 */
public class CropFinderEnhanced extends Strategy {
	
	private final static Logger log = Logger.getLogger(CropFinderEnhanced.class);
	private int startX = -9999, startY = -9999;			// start location invalid
    private int fromX = -9999, fromY = -9999;			// as is distance from location
    private int maxRange = 0;							// maximun search in squares
    private int lastX = -9999, lastY = -9999;			// start location - use invalid co-ordinates
    private int sCount = 0;								// count of grids checked, could use for state, set -1 if finished
    
    private enum State {
    	CREATED, RUNNING, FINISHED
    };
    private State state = State.CREATED;				// separate state
    private OutputType outputType = OutputType.TXT;		// output file type
	
	@Override
	public TimeWhenRunnable execute() throws ConversationException,
			InvalidConfigurationException {
		/*  strategy configuration - start and from are optional and default to location of village owning strategy
        <strategy class="CropFinder" desc="Find 9 and 15 Croppers around given location" enabled="true"  maxRadius="10" minPauseMinutes="2" uid="sV1cf">
        <start x="XCOORD" y="YCOORD" maxRadius="10" minPauseMinutes="2"/>
        <from  x="XCOORD" y="YCOORD" />
        <output append="true" format="csv" />
        </strategy>
        
        Max radius uses coordinate units. It starts from the given x and y point.
         */
		
		log.info("Executing strategy " + super.getDesc());
        NDC.push(super.getDesc());
		
        /*
         * Strategy currently does not support resume. In other words it finds the cropper one shot.
         * Should the performance of searching takes more than a minute, I'll consider about it.
         * 
         * zepx
         */
        try {
            log.debug("CropFinder from Village " + this.village.getVillageName() + " (" + this.village.getPosition().x + "," + this.village.getPosition().y + ")");
			// explore map - returns true if more to do
            /*
        	if (explore(this.village, super.config, super.util)) {
                log.info(String.format("Strategy %s done for now", getDesc()));
                Long minPauseMinutes = super.config.getLong("/@minPauseMinutes", super.config.getLong("/start/@minPauseMinutes", 0));
                return new TimeWhenRunnable(System.currentTimeMillis() + (minPauseMinutes * Util.MILLI_MINUTE), true); // Try again later
        	}
        	*/
            explore(this.village, super.config, super.util);
            // finished or errors set a long time or disable this strategy
            log.info(String.format("Strategy %s Finished", getDesc()));
            village.strategyDone.setFinished(this.getId(), true); //register it as done
            return TimeWhenRunnable.NEVER;
        } finally {
            NDC.pop();
        }
	}
	
	
	public void explore(Village village, SubnodeConfiguration config, Util util) throws ConversationException {
		List<JsonTile> tiles;
		TravianMap map = new TravianMap(util, village.getTranslator());
		
		//Obtain initial configurations
		Coordinates startCoord = new Coordinates(config, "start", 999, 999);
		startX = startCoord.getIntX();
		startY = startCoord.getIntY();
		maxRange = config.getInt("/start/@maxradius", 1);
		maxRange = config.getInt("/@maxRadius", config.getInt("/start/@maxRadius", maxRange));
		//If start location is not specified, x = 999 or y = 999. Obtain it automatically.
		if(startX == 999 || startY == 999) {
			startX = village.getPosition().x;
			startY = village.getPosition().y;
			EventLog.log("Coordinates invalid or absent. Using current village's ones: " + startX + "," + startY);
		}
		
		// get origin to calculate distance from, default is the same as start
		//Coordinates coord = new Coordinates(config, "from", startX, startY);
		//fromX = coord.getIntX();
		//fromY = coord.getIntY();
		
		// get output file format 0 used as file extension
        String outputExt = config.getString("/output/@format", "");
        if (outputExt != "") {
            outputType = OutputType.fromKey(outputExt);
        }
        
        // as each map block is written it will be appended to the output file
        // however at the start of the strategy allow user to start a clean file
        Boolean fileAppend = config.getBoolean("/output/@append", true);
        // to use this as dont want to expose filename outside 7x7 class need to create an instance and clear if needed
        // TODO - consider if file ownership should be other way round and this passes into 7x7 class
        if (!fileAppend) {
            // SevenBySeven map = new SevenBySeven(super.util, village.getVillageName(), village.getVillageUrlString(), village.getTranslator());
            map.setOutputType(outputType, fileAppend);
        }
        log.debug("Village " + village.getVillageName() + " URL " + village.getVillageUrlString());
        EventLog.log("Start Searching Around " + startX + ", " + startY + " max Range " + maxRange);
        if (!fileAppend) {
            EventLog.log("Results will overwrite ." + outputType.toString() + " file");
        }

        // first time
        //lastX = (int) (startX - (Math.floor(maxRange / 7) * 7));
        //lastY = (int) (startY - (Math.floor(maxRange / 7) * 7));
        
        /* Calculate the number of times we need to shift the x and y. Assuming we use the largest
         * zoom possible.
         */
        boolean finish = false; 	
        tiles = map.fetchRadius(startX, startY, maxRange);
        	
    	ArrayList<JsonTile> cropperTiles = new ArrayList<JsonTile>();
    	for(JsonTile tile : tiles) {   		
    		
    		if(tile.isValley()) {
    			if(tile.getValleyType() == ValleyType.FIELD1 ||
    			tile.getValleyType() == ValleyType.FIELD6) {
    				cropperTiles.add(tile);
    				System.out.println(tile.x() + " " + tile.y());
    			}
    		}
    	}
    	
    	return;
    	
	}
	
	/*public boolean explore(Village village, SubnodeConfiguration config, Util util) throws ConversationException {
		ArrayList<JsonTile> tiles;	
		
		//If strategy is has not run before, create it.
		if(state == State.CREATED) {
			// get start location
        	Coordinates startCoord = new Coordinates(config, "start", 999, 999);
        	startX = startCoord.getIntX();
        	startY = startCoord.getIntY();
            // allow maxRadius on top or start line & also old version which did not have capital!
            maxRange = config.getInt("/start/@maxradius", 1);
            maxRange = config.getInt("/@maxRadius", config.getInt("/start/@maxRadius", maxRange));
            // start by getting this start location, only need to do this if start not specified
            if (startX == 999) {
                // village co-ords only set after first update so make sure villages run at least once
                startX = village.getPosition().x;
                startY = village.getPosition().y;
                EventLog.log("Coordinates invalid or absent. Using current village's ones: " + startX + "," + startY);
                // System.exit(0);
            }
            // get origin to calculate distance from, default is the same as start
			Coordinates coord = new Coordinates(config, "from", startX, startY);
			fromX = coord.getIntX();
			fromY = coord.getIntY();

            // get output file format 0 used as file extension
            String outputExt = config.getString("/output/@format", "");
            if (outputExt != "") {
                outputType = OutputType.fromKey(outputExt);
            }
            // as each map block is written it will be appended to the output file
            // however at the start of the strategy allow user to start a clean file
            Boolean fileAppend = config.getBoolean("/output/@append", true);
            // to use this as dont want to expose filename outside 7x7 class need to create an instance and clear if needed
            // TODO - consider if file ownership should be other way round and this passes into 7x7 class
            if (!fileAppend) {
                // SevenBySeven map = new SevenBySeven(super.util, village.getVillageName(), village.getVillageUrlString(), village.getTranslator());
                TravianMap map = new TravianMap(util, village.getTranslator());
                map.setOutputType(outputType, fileAppend);
            }
            log.debug("Village " + village.getVillageName() + " URL " + village.getVillageUrlString());
            EventLog.log("Start Searching Around " + startX + ", " + startY + " max Range " + maxRange);
            if (!fileAppend) {
                EventLog.log("Results will overwrite ." + outputType.toString() + " file");
            }

            // first time
            lastX = (int) (startX - (Math.floor(maxRange / 7) * 7));
            lastY = (int) (startY - (Math.floor(maxRange / 7) * 7));
            // everything initialised
            state = State.RUNNING;
		}
		
		// check if something to do
        if (state == State.RUNNING) {
            // create a new 7x7 for this search - they are not kept but perhaps should be or else check in output file
            // Valley	valley = new Valley(super.util, village.getVillageName(), village.getVillageUrlString(), village.getTranslator());
        	// village.getVillageName(); 
        	// village.getVillageUrlString(); 
        	// village.getTranslator();
            TravianMap map = new TravianMap(util, village.getTranslator());
            sCount++;
            // set output if not default
            if (outputType != OutputType.TXT) {
                map.setOutputType(outputType);
            }

            EventLog.log("Search Map " + sCount + " from " + lastX + ", " + lastY);
            // note if we get a page error then skips and repeats whole block - is this the desired action?
            map.search(util, lastX, lastY, fromX, fromY);

            // go back to main page - simpler for other routines and also emulate person checking what happening
            log.trace("Going Back to Main Page");
            village.gotoMainPage();

            // advance along row and check if finished
            lastX += 7;
            if (lastX > (startX + maxRange)) {
                // step Y down a 7x7 check if finished
                lastY += 7;
                if (lastY > (startY + maxRange)) {
                    // mark done
                    lastX = lastY = -999;
                    state = State.FINISHED;
                    EventLog.log("Finished Searching");
                } else {
                    lastX = startX - maxRange;
                }
            }
            log.debug("Cropfinder NextX " + Integer.toString(lastX) + " NextY " + Integer.toString(lastY));
        }
        // check if more for next time
        if (state != State.FINISHED) {
            // log.info(String.format("Strategy %s done for now", getDesc()));
            // Long minPauseMinutes = super.config.getLong("/@minPauseMinutes", super.config.getLong("/start/@minPauseMinutes", 2));
            // return new TimeWhenRunnable(System.currentTimeMillis() + (minPauseMinutes * Util.MILLI_MINUTE), true); // Try again later
            return true;
        } else {
        	log.trace("Finished Exploring");
            // set a long time or disable this strategy
            // log.info(String.format("Strategy %s Finished", getDesc()));
            // village.strategyDone.setFinished(this.getId(), true); //register it as done
            // return TimeWhenRunnable.NEVER;
        }
        // return TimeWhenRunnable.NEVER;
        return false;
		
		return false;
	}*/

}
