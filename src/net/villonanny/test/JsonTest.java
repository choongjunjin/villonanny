package net.villonanny.test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.lang.reflect.Type;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.InstanceCreator;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class JsonTest {
	
	public static void main(String args[]) {	
		BufferedReader reader;
		try {
			reader = new BufferedReader(new FileReader("sample2.txt"));
			JsonObject obj = new JsonParser().parse(reader).getAsJsonObject();
			JsonObject obj2 = obj.getAsJsonObject("data");
			
			System.out.println(obj2.get("formular").getAsString());
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	public static void test1() {
		try {
			BufferedReader reader = new BufferedReader(new FileReader("sample.txt"));
			JsonObject obj = new JsonParser().parse(reader).getAsJsonObject();
			JsonArray array = obj.getAsJsonObject("data").getAsJsonArray("tiles");
			
			GsonBuilder gsonBuilder = new GsonBuilder();
			gsonBuilder.registerTypeAdapter(TileTest.class, new TileInstance());
			
			Gson json = gsonBuilder.create();
			TileTest[] tiles = json.fromJson(array, TileTest[].class);
			System.out.println("Done");
			/*for(JsonElement e : array) {
				System.out.println("x: " + e.getAsJsonObject().get("x") + " y: " + e.getAsJsonObject().get("y"));
			}*/
			
			//for(Tile t : tiles) {
			//	System.out.println(t);
			//}
			//System.out.println("Tiles: " + tiles.length);
			
		} catch (FileNotFoundException e) {			
			e.printStackTrace();
		}
	}
	
	public static class TileTest {
		private int x, y, u, a, d;		
		private String c;
		private String t;
		//private String test = "Test";
		
		public TileTest(String msg) {
			System.out.println(msg);
		}
		
		@Override
		public String toString() {
			return " " + x + " " + y + " " + u + " " + a + " " + c + " " + t;
		}
	}
	
	public static class TileInstance implements InstanceCreator<TileTest> {

		@Override
		public TileTest createInstance(Type type) {
			return new TileTest("Instance is created");
		}
		
	}

}
