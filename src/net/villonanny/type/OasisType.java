package net.villonanny.type;

public enum OasisType {
	
	TYPE1(ResourceType.WOOD, null, 25, 0),
	TYPE2(ResourceType.WOOD, ResourceType.CROP, 25, 25),
	TYPE3(ResourceType.CLAY, null, 25, 0),
	TYPE4(ResourceType.CLAY, ResourceType.CROP, 25, 25),
	TYPE5(ResourceType.IRON, null, 25, 0),
	TYPE6(ResourceType.IRON, ResourceType.CROP, 25, 25),
	TYPE7(ResourceType.CROP, null, 50, 0),
	TYPE8(ResourceType.CROP, null, 25, 0),
	;
	
	private ResourceType type1, type2;
	private int stat1, stat2;
	
	private OasisType(ResourceType type1, ResourceType type2, int percent1, int percent2) {
		this.type1 = type1;
		this.type2 = type2;
		this.stat1 = percent1;
		this.stat2 = percent2;
	}
	
	private OasisType(String type1, String type2, int percent1, int percent2) {
		this(ResourceType.fromString(type1), ResourceType.fromString(type2), percent1, percent2);
	}
	
	public ResourceType getType1() {
		return type1;
	}
	
	public ResourceType getType2() {
		return type2;
	}
	
	public int getStat1() {
		return stat1;
	}
	
	public int getStat2() {
		return stat2;
	}
	
	public String toString() {
		String merge = "+" + stat1 + "% " + type1;
		
		if(type2 != null) {
			merge += " and +" + stat2 + "% " + type2; 
		}
		
		return merge += " per hour";
	}
	
	public static OasisType fromString(String type1, String type2) {
		for(OasisType ot : OasisType.values()) {
			if(type1.equalsIgnoreCase(ot.getType1().toString())) {
				if(ot.getType2() == null && type2.isEmpty())
					return ot;
				else if(ot.getType2() != null && type2.equalsIgnoreCase(ot.getType2().toString()))
					return ot;
			}
		}
		return null;
	}
	
	public static OasisType fromString(ResourceType type1, ResourceType type2) {		
		return fromString(type1.toString(), type2 == null ? "" : type2.toString());
	}
	
}
