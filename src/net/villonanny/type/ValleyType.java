package net.villonanny.type;

public enum ValleyType {
	
	//Wood, Clay, Iron, Crop
	FIELD1(3, 3, 3, 9),
	FIELD2(3, 4, 5, 6),
	FIELD3(4, 4, 4, 6),
	FIELD4(4, 5, 3, 6),
	FIELD5(5, 3, 4, 6),
	FIELD6(1, 1, 1, 15),
	FIELD7(4, 4, 3, 7),
	FIELD8(3, 4, 4, 7),
	FIELD9(4, 3, 4, 7),
	FIELD10(3, 5, 4, 6),
	FIELD11(4, 3, 5, 6),
	FIELD12(5, 4, 3, 6)
	;
	
	private int wood, clay, iron, crop;
	
	private ValleyType(int wood, int clay, int iron, int crop) {
		this.wood = wood;
		this.clay = clay;
		this.iron = iron;
		this.crop = crop;
	}
	
	public int getWood() {
		return wood;
	}
	
	public int getClay() {
		return clay;
	}
	
	public int getIron() {
		return iron;
	}
	
	public int getCrop() {
		return crop;
	}
	
	public String toString() {
		return wood + "-" + clay + "-" + iron + "-" + crop;
	}
	
	public static ValleyType fromInt(int i) {
		return ValleyType.valueOf("FIELD" + i);
	}

}
